<?php
include ('crudInterface.php');
include ('repositoryInterface.php');


abstract class Dao implements Crud, Repository
{
    protected $pdo;


    // CONNEXION A LA BASE DE DONNEE
    public function __construct()
    {
        $jsonDecode = json_decode(file_get_contents("config/database.json"));
        $driver = $jsonDecode->driver;
        $host = $jsonDecode->host;
        $dbname = $jsonDecode->dbname;
        $username = $jsonDecode->username;
        $password = $jsonDecode->password;


        try {
            $this->pdo = new PDO("$driver:host=$host;dbname=$dbname", $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo 'connexion ok<br>';
        } catch (PDOException $e) {
            echo 'Echec connexion' . $e->getMessage();
        }
    }




    // CREER
    abstract public function create($key, $value);
    // RECUPERER 
    abstract public function retrieve($id);
    // MODIFIER
    abstract public function update($id);
    // SUPPRIMER 
    abstract public function delete($id);
    abstract public function getAll($table);
    // tableau associatif pour les clauses WHERE et AND des requestes SQL
    abstract public function getAllBy();

}

