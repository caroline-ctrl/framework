<?php
interface Crud{
    public function create($key, $value);
    public function retrieve($id);
    public function update($id);
    public function delete($id);
}