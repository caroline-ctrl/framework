<?php
interface Repository{
    public function getAllBy();
    public function getAll($table);
}